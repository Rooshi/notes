CC=clang
CFLAGS=-g -Wall
TARGET=notes

default: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c

clean:
	rm notes
